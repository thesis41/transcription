import numpy as np
import cv2 as cv
import pytesseract

dilated_path = "images/out/dilated/"
separators_path = "images/out/separators/"
pages = [
            "pe-02.ppm",
            "pe-03.ppm",
            "pe-04.ppm",
            "pe-05.ppm",
            "pe-06.ppm",
            "pe-07.ppm",
            "pe-08.ppm",
            "pe-09.ppm",
            "pe-10.ppm"
        ]
edit_filename = "tmp.txt"
out_filename = "res/out.txt"
whitelist_chars = r'123456789@ABCDEFGHIJKLMNOPQRSTUVWXYZ()/ '
tes_config = '--dpi 300 -c tessedit_char_whitelist=\"' + whitelist_chars + '\"'

def i3imshow(wname, img):
    cv.namedWindow(wname, cv.WINDOW_NORMAL)
    while True:
        cv.imshow(wname, img)
        k = cv.waitKey(33)
        # Esc key to stop
        if k == 27:
            cv.destroyAllWindows()
            return

def get_separators(p):
    separators = []
    for i, row in enumerate(p):
        if np.all(row == [255,0,0]):
            separators.append(i)
    return separators

def transcribe_line(img, s1, s2, i, page):
    line = img[s1:s2]
    text = correct(pytesseract.image_to_string(line, config=tes_config))
    ef = open(edit_filename, 'w+')
    ef.write(text[:-1])
    ef.flush()
    ef.close()
    i3imshow(f"line {i}", line)
    ef = open(edit_filename, 'r+')
    res = ef.read()
    of = open(out_filename, 'a+')
    of.write(res)
    of.flush()
    ef.close()
    of.close()

def correct(s):
    replacements = [
            ("ALPHAL", "ALPHA1"),
            (" AL ", " A1 "),
            ("(AL ", "(A1 "),
            ("AL) ", "(A1)"),
            ("9UOTE","QUOTE"),
            ("LTIST","LIST"),
            ("CAND ","(AND"),
            ("QUDTE","QUOTE"),
            ("CATOM","(ATOM"),
            ("RN","STK"),
            ("NRTVL","NRTV1"),
            ("Z2","2"),
            ("QA", "BT2"),
            ("AS", "A5"),
            ("CUNS", "CONS"),
            ("2Q", "EQ"),
            ("A5SOC", "ASSOC"),
            ("RETUSTK", "RETURN")
            ]
    res = s
    for pair in replacements:
        res = res.replace(pair[0], pair[1])
    return res

if __name__ == "__main__":
    for i, p in enumerate(pages):
        of = open(out_filename, 'a+')
        of.flush()
        of.close()
        seps_img = cv.imread(separators_path+p)
        img = cv.imread(dilated_path+p)
        seps = get_separators(seps_img)

        for i in range(len(seps[:-1])):
            transcribe_line(img, seps[i], seps[i+1], i, p)


