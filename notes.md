# Convert PDF pages to ppm images

## PDF to PPM - density 300:

```
pdftoppm -r 300 SelfApplicablePE.pdf pe
```

# Clean images

Clean the text rows to the left and right of the text, and remove hand written
comments
