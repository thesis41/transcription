;;;C PARTIAL EVALUATION ALGORITHM ALPHA1 PROGRAMMED BY YFUTAMURA 1/30/69
(DEFLIST ((PROG2 (LAMBDA (X A)
                         (COND ((EVAL (CAR X) A) (EVAL (CADR X) A))
                               (T (EVAL (CADR X) A)))))) FEXPR)
(DEFINE (
         (ALPHA1 (LAMBDA (A1 A2 A3 A4)
                         (BT A1 A2 A3 A4 NIL NIL)))
         (BT (LAMBDA (A1 A2 A3 A4 A5 A6)
                     (COND
                       ((NULL A2) (LIST (QUOTE LAMBDA) NIL
                                        (LIST (QUOTE QUOTE) (APPLY A1 A4 A5))))
                       ((AND (ATOM A1) (GET A1 (QUOTE EXPR)))
                        (BT (GET A1 (QUOTE EXPR)) A2 A3 A4 A5 A6))
                       ((ATOM A1) (BT (EVAL A1 A5) A2 A3 A4 A5 A6))
                       ((EQ (CAR A1) (QUOTE LAMBDA))
                        (BT1 (STK A1 A3 A6) (GENSYM) A1 A2 A3 A4 A5 A6)
                        )
                       (T (BT2 A1 A2 A3 A4 A5 A6)) )))
         (BT1 (LAMBDA (X Y A1 A2 A3 A4 A5 A6)
                      (COND
                        ((NULL X)
                         (CDLL Y
                               (BT2 A1 A2 A3 A4 A5 (CONS (CONS (CONS A1 A3)
                                                               (LIST (CONS A4 Y))) A6))) )
                        ((NULL (ASSOC A4 (CDR X))) (CDLL
                                                     Y (BT2 A1 A2 A3 A4 A5
                                                            (PROG2 (NCONC X (LIST (CONS A4 Y))) A6))))
                        (T (CDR (ASSOC A4 (CDR X)))) )) )
         (STK (LAMBDA (A1 A3 A6)
                      (ASSOC (CONS A1 A3) A6) ))
         (NRTV  (LAMBDA (A1 A2)
                        (COND
                          ((ATOM A1) (NOT (MEMBER A1 A2)))
                          ((NUMBERP A1) T)
                          ((EQ (CAR A1) (QUOTE QUOTE)) T)
                          ((EQ (CAR A1) (QUOTE COND)) (NRTV1 (CDR A1) A2))
                          ((EQ (CAR A1) (QUOTE GENSYM)) NIL)
                          (T (NRTV2 (CDR A1) A2)) )))
         (NRTV1 (LAMBDA (X A2) (COND
                                 ((NULL X) T)
                                 ((AND(NRTV (CAAR X) A2) (NRTV (CADAR X) A2))
                                  (NRTV1 (CDR X) A2))
                                 (T NIL) )))
         (NRTV2 (LAMBDA (X A2) (COND
                                 ((NULL X) T)
                                 ((NRTV (CAR X) A2) (NRTV2 (CDR X) A2))
                                 (T NIL) )))
         (BT2 (LAMBDA (A1 A2 A3 A4 A5 A6) (COND
                                            ((EQ (CAR A1) (QUOTE LAMBDA))
                                             (LIST (QUOTE LAMBDA) A2 (BT3 (CADDR A1) A2 A3 A4
                                                                          A5 A6)) )
                                            ((EQ (CAR A1) (QUOTE LABEL))
                                             (BT (CADDR A1) A2 A3 A4
                                                 (CONS (CONS (CADR A1) (CADDR A1)) A5) A6))
                                            )))
         (BT3 (LAMBDA (A1 A2 A3 A4 A5 A6) (COND
                                            ((NRTV A1 A2)
                                             (LIST (QUOTE QUOTE) (EVAL A1 (PAIRLIS A3 A4 NIL))) )
                                            ((MEMBER A1 A2) A1)
                                            ((ATOM (CAR A1))
                                             (COND
                                               ((OR
                                                  (EQ (CAR A1) (QUOTE ASSOC))
                                                  (EQ (CAR A1) (QUOTE RLIST2))
                                                  (EQ (CAR A1) (QUOTE COPY1))
                                                  (EQ (CAR A1) (QUOTE NRTV))
                                                  (EQ (CAR A1) (QUOTE RTVL1))
                                                  (EQ (CAR A1) (QUOTE CTVL1))
                                                  (EQ (CAR A1) (QUOTE CLIST))
                                                  (EQ (CAR A1) (QUOTE PAIRLIS))
                                                  (EQ (CAR A1) (QUOTE PPAIR))
                                                  (EQ (CAR A1) (QUOTE GOLIST))
                                                  (EQ (CAR A1) (QUOTE STK))
                                                  (EQ (CAR A1) (QUOTE PROG2))
                                                  (EQ (CAR A1) (QUOTE GENSYM))
                                                  (EQ (CAR A1) (QUOTE CDLL))
                                                  )
                                                (CONS (CAR A1) (RLIST (CDR A1) A2 A3 A4 A5 A6)))
                                               ((GET (CAR A1) (QUOTE EXPR))
                                                (BT3 (CONS (GET (CAR A1) (QUOTE EXPR)) (CDR A1))
                                                     A2 A3 A4 A5 A6))
                                               ((EQ (CAR A1) (QUOTE COND))
                                                (BT4 (CDR A1) A2 A3 A4 A5 A6))
                                               ((OR (GET (CAR A1) (QUOTE SUBR)) (GET (CAR A1) (QUOTE FSUBR))
                                                    )
                                                (CONS (CAR A1) (RLIST (CDR A1) A2 A3 A4 A5 A6)))
                                               ((ASSOC (CAR A1) A5)
                                                (BT3 (CONS (CDR (ASSOC (CAR A1) A5)) (CDR A1))
                                                     A2 A3 A4 A5 A6))
                                               (T (ERROR A1)) ))
                                            ((EQ (CAAR A1) (QUOTE LAMBDA))
                                             (CONS (SRCHSTK (CAR A1) (CADAR A1)
                                                            (CDR A1)
                                                            A2 A3 A4 A5 A6)
                                                   (RLIST (RLIST2 (CDR A1) A2) A2 A3 A4 A5 A6) ))
                                            )))
         (RLIST (LAMBDA (X A2 A3 A4 A5 A6) (COND
                                             ((NULL X) NIL)
                                             (T (CONS (BT3 (CAR X) A2 A3 A4 A5 A6)
                                                      (RLIST (CDR X) A2 A3 A4 A5 A6))) )))
         (RLIST1 (LAMBDA (X A2 A3 A4 A5 A6) (COND
                                              ((NULL X) NIL)
                                              ((NRTV (CAR X) A2)
                                               (CONS (EVAL (CAR X) (PAIRLIS A3 A4 NIL))
                                                     (RLIST1 (CDR X) A2 A3 A4 A5 A6)))
                                              (T (CONS (BT3 (CAR X) A2 A3 A4 A5 A6)
                                                       (RLIST1 (CDR X) A2 A3 A4 A5 A6))) )))
         (RLIST2 (LAMBDA (X A2) (COND
                                  ((NULL X) NIL)
                                  ((NRTV (CAR X) A2) (RLIST2 (CDR X) A2))
                                  (T (CONS (CAR X) (RLIST2 (CDR X) A2))) )))
         (SRCHSTK (LAMBDA (X Y Z A2 A3 A4 A5 A6)
                          (BT X (RTVL1 Z Y A2) (CTVL1 Z Y A2)
                              (EVLIS (CLIST Z A2) (PAIRLIS A3 A4 A5)) A5 A6) ))
         (RTVL1 (LAMBDA (Z Y A2) (COND
                                   ((NULL Z) NIL)
                                   ((NRTV (CAR Z) A2) (RTVL1 (CDR Z) (CDR Y) A2))
                                   (T (CONS (CAR Y) (RTVL1 (CDR Z) (CDR Y) A2))) )))
         (CTVL1 (LAMBDA (Z Y A2) (COND
                                   ((NULL Z) NIL)
                                   ((NRTV (CAR Z) A2)
                                    (CONS (CAR Y) (CTVL1 (CDR Z) (CDR Y) A2)))
                                   (T (CTVL1 (CDR Z) (CDR Y) A2)) )))
         (CLIST (LAMBDA (Z A2) (COND
                                 ((NULL Z) NIL)
                                 ((NRTV (CAR Z) A2)
                                  (CONS (CAR Z) (CLIST (CDR Z) A2)))
                                 (T (CLIST (CDR Z) A2)) )))
         (BT4 (LAMBDA (X A2 A3 A4 A5 A6) (COND
                                           ((NRTV (CAAR X) A2)
                                            (COND
                                              ((EVAL (CAAR X) (PAIRLIS A3 A4 A5))
                                               (BT3 (CADAR X) A2 A3 A4 A5 A6))
                                              (T (BT4 (CDR X) A2 A3 A4 A5 A6)) ))
                                           (T (CONS (QUOTE COND) (COND1 X A2 A3 A4 A5 A6))) )))
         (COND1 (LAMBDA (X A2 A3 A4 A5 A6) (COND
                                             ((NULL (CDR X))
                                              (LIST (LIST (BT5 (CAAR X) A2 A3 A4 A5 A6)
                                                          (BT5 (CADAR X) A2 A3 A4 A5 A6))) )
                                             (T (CONS (LIST (BT5 (CAAR X) A2 A3 A4 A5 A6)
                                                            (BT5 (CADAR X) A2 A3 A4 A5 A6))
                                                      (COND1 (CDR X) A2 A3 A4 A5 A6))) )))
         (BT5 (LAMBDA (X A2 A3 A4 A5 A6) (COND
                                           ((NRTV X A2) (COND
                                                          ((ATOM X) (LIST (QUOTE QUOTE) (EVAL X (PAIRLIS
                                                                                                  A3 A4 A5))))
                                                          ((EQ (CAR X) (QUOTE QUOTE)) X)
                                                          ((EQ (CAR X) (QUOTE COND))
                                                           (COND2 (CDR X) A2 A3 A4 A5 A6))
                                                          (T (CONS (CAR X) (CEVLIS (CDR X) A2 A3 A4 A5 A6))) ))
                                           ((EQ (CAR X) (QUOTE COND))
                                            (COND2 (CDR X) A2 A3 A4 A5 A6))
                                           (T (BT3 X A2 A3 A4 A5 A6)) )))
         (COND2 (LAMBDA (Y A2 A3 A4 A5 A6) (COND
                                             ((NRTV (CAAR Y) A2) (COND
                                                                   ((EVAL (CAAR Y) (PAIRLIS A3 A4 A5))
                                                                    (BT5 (CADAR Y) A2 A3 A4 A5 A6))
                                                                   (T (COND2 (CDR Y) A2 A3 A4 A5 A6)) ))
                                             (T (CONS (QUOTE COND) (COND1 Y A2 A3 A4 A5 A6))) )))
         (CEVLIS (LAMBDA (Y A2 A3 A4 A5 A6) (COND
                                              ((NULL Y) NIL)
                                              (T (CONS (BT5 (CAR Y) A2 A3 A4 A5 A6)
                                                       (CEVLIS (CDR Y) A2 A3 A4 A5 A6))) )))
         (PAIRLIS (LAMBDA (X Y A) (COND
                                    ((NULL X) A)
                                    (T (CONS (CONS (CAR X) (CAR Y))
                                             (PAIRLIS (CDR X) (CDR Y) A))) )))
         (CDLL (LAMBDA (X Y) (CAR (DEFINE (LIST
                                            (LIST X Y)))) ))
         (GENSYM (LAMBDA NIL
                         (PROG (U)
                               (SETQ U (CAR GENSYM))
                               (CSETQ GENSYM (CDR GENSYM))
                               (RETURN U)  )))
         (ASSOC (LAMBDA (X A) (COND ((NULL A) NIL) ((EQUAL (CAAR A) X) (CAR
                                                                         A)) (T (ASSOC X (CDR A))))))
         ;;;C END OF P.E. ALGORITHM
         ;;;C PROG INTERPRETER PROGF
         (PROGF (LAMBDA (X A)
                        (PFEVAL (CDR X) (PPAIR (CAR X) A) (GOLIST (CDR X) NIL)
                                ) ))
         (PPAIR (LAMBDA (X A) (COND
                                ((NULL X) A)
                                (T (CONS (CONS (CAR X) NIL) (PPAIR (CDR X) A))) )))
         (GOLIST (LAMBDA (X A) (COND
                                 ((NULL X) A)
                                 ((ATOM (CAR X)) (CONS X (GOLIST (CDR X) A)))
                                 (T (GOLIST (CDR X) A)) )))
         (PFEVAL (LAMBDA (X A G) (COND
                                   ((NULL X) NIL)
                                   ((ATOM (CAR X)) (PFEVAL (CDR X) A G))
                                   ((EQ (CAAR X) (QUOTE GO))
                                    (PFEVAL (CDR (ASSOC (CADAR X) G)) A G))
                                   ((EQ (CAAR X) (QUOTE COND))
                                    (PFCOND (CONS (CDAR X) (CDR X)) A G))
                                   ((EQ (CAAR X) (QUOTE SETQ))
                                    (PROG2 (RPLACD (ASSOC (CADAR X) A)
                                                   (EVAL (CADDR (CAR X)) A))
                                           (PFEVAL (CDR X) A G)))
                                   ((EQ (CAAR X) (QUOTE SET))
                                    (PROG2 (RPLACD (ASSOC (EVAL (CADAR X) A) A)
                                                   (EVAL (CADDR (CAR X)) A))
                                           (PFEVAL (CDR X) A G)))
                                   ((EQ (CAAR X) (QUOTE RETURN)) (EVAL (CADAR X) A))
                                   (T (PROG2 (EVAL (CAR X) A)
                                             (PFEVAL (CDR X) A G))) )))
         (PFCOND (LAMBDA (Y A G) (COND
                                   ((NULL (CAR Y)) (PFEVAL (CDR Y) A G))
                                   ((EVAL (CAAAR Y) A) (COND
                                                         ((EQ (CAADR (CAAR Y)) (QUOTE GO))
                                                          (PFEVAL (CDR (ASSOC (CADAR (CDAAR Y)) G)) A G))
                                                         ((EQ (CAADR (CAAR Y)) (QUOTE SETQ))
                                                          (PROG2 (RPLACD (ASSOC (CADAR (CDAAR Y)) A)
                                                                         (EVAL (CADDR (CADAR (CAR Y))) A))
                                                                 (PFEVAL (CDR Y) A G)))
                                                         ((EQ (CAADR (CAAR Y)) (QUOTE SET))
                                                          (PROG2 (RPLACD (ASSOC (EVAL (CADAR (CDAAR Y)) A) A)
                                                                         (EVAL (CADDR (CADAR (CAR Y))) A))
                                                                 (PFEVAL (CDR Y) A G)))
                                                         ((EQ (CAADR (CAAR Y) ) (QUOTE RETURN))
                                                          (EVAL (CADAR (CDAAR Y)) A))
                                                         (T (PROG2 (EVAL (CADAR (CAR Y)) A)
                                                                   (PFEVAL (CDR Y) A G))) ))
                                   (T (PFCOND (CONS (CDAR Y) (CDR Y)) A G)) )))
         ))
(CSET GENSYM1
      (G1 G2 G3 G4 G5 G6 G7 G8 G9 G10 G11 G12 G13 G14 G15 G16 G17
          G18 G19 G20 G21 G22 G23 G24 G25 G26 G27 G28 G29 G30 G31
          G32 G33 G34 G35 G36 G37 G38 G39 G40 G41 G42 G43 G44 G45
          G46 G47 G48 G49 G50 G51 G52 G53 G54 G55 G56 G57 G58 G59
          G60 G61 G62 G63 G64 G65 G66 G67 G68 G69 G70 G71 G72 G73
          G74 G75 G76 G77 G78 G79 G80 G81 G82 G83 G84 G85 G86 G87
          G88 G89 G90 G91 G92 G93 G94 G95 G96 G97 G98 G99 G100 G101
          G102 G103 G104 G105 G106 G107 G108 G109 G110 G111 G112
          G113 G114 G115 G116 G117 G118 G119 G120 G121 G122 G123
          G124 G125 G126 G127 G128 G129 G130 G131 G132 G133 G134
          G135 G136 G137 G138 G139 G140 G141 G142 G143 G144 G145
          G146 G147 G148 G149 G150 G151 G152 G153 G154 G155 G156
          G157 G158 G159 G160 G161 G162 G163 G164 G165 G166 G167
          G168 G169 G170 G171 G172 G173 G174 G175 G176 G177 G178
          G179 G180 G181 G182 G183 G184 G185 G186 G187 G188 G189
          G190 G191 G192 G193 G194 G195 G196 G197 G198 G199 G200
          G201 G202 G203 G204 G205 G206 G207 G208 G209 G210
          G211 G212 G213 G214 G215 G216 G217 G218 G219 G220
          G221 G222 G223 G224 G225 G226 G227 G228 G229 G230
          G231 G232 G233 G234 G235 G236 G237 G238 G239 G240
          G241 G242 G243 G244 G245 G246 G247 G248 G249 G250
          G251 G252 G253 G254 G255 G256 G257 G258 G259 G260
          G261 G262 G263 G264 G265 G266 G267 G268 G269 G270
          G271 G272 G273 G274 G275 G276 G277 G278 G279 G280
          G281 G282 G283 G284 G285 G286 G287 G288 G289 G290
          G291 G292 G293 G294 G295 G296 G297 G298 G299 G300
          G301 G302 G303 G304 G305 G306 G307 G308 G309 G310
          G311 G312 G313 G314 G315 G316 G317 G318 G319 G320
          G321 G322 G323 G324 G325 G326 G327 G328 G329 G330
          G331 G332 G333 G334 G335 G336 G337 G338 G339 G340
          G341 G342 G343 G344 G345 G346 G347 G348 G349 G350
          G351 G352 G353 G354 G355 G356 G357 G358 G359 G360
          G361 G362 G363 G364 G365 G366 G367 G368 G369 G370
          G371 G372 G373 G374 G375 G376 G377 G378 G379 G380
          G381 G382 G383 G384 G385 G386 G387 G388 G389 G390
          G391 G392 G393 G394 G395 G396 G397 G398 G399 G400))
(CSETQ GENSYM GENSYM1)
;;;C TEST PROGRAM FOR TERMINATION OF ALGOL CUMPILER GENERATION
(PROG (A COMP OBJ1 OBJ2 X)
      (SETQ COMP (ALPHA1 (QUOTE ALPHA1) (QUOTE (A4))
                         (QUOTE (A1 A2 A3))
                         (QUOTE (PROGF (A) (X)))))
      (PRINT (QUOTE ***SUCCESSFUL-COMPILER-GENERATON***))
      (PRINT (QUOTE ***GENERATED-COMPILER-IS***))
      (PRINT (GET COMP (QUOTE EXPR)))
      L1 (SETQ X (READ))
      (COND ((NULL X) (RETURN (QUOTE ***END***))))
      (SETQ A (READ))
      (PRINT (QUOTE ***VALUE-OF-X-IS***))
      (PRINT (PROGF X A))
      (SETQ OBJ1 (ALPHA1 (QUOTE PROGF) (QUOTE (A)) (QUOTE (X))
                         (LIST X)))
      (PRINT (QUOTE ***VALUE-OBJ1-IS***))
      (PRINT (APPLY OBJ1 (LIST A) NIL))
      (SETQ OBJ2 (APPLY COMP (LIST (LIST X)) NIL))
      (PRINT (QUOTE ***SUCCESSFUL-COMPILATION***))
      (PRINT (QUOTE ***OBJ2-IS***))
      (PRINT (GET OBJ2 (QUOTE EXPR)))
      (MAPCAR GENSYM1
              (FUNCTION (LAMBDA (U)
                                (PRINT (GET U (QUOTE EXPR))))) )
      (PRINT (QUOTE ***VALUE-OF-OBJ2-IS***))
      (PRINT (APPLY OBJ2 (LIST A) NIL))
      (GO L1) )
STOP)))
